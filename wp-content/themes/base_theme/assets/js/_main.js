/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can 
 * always reference jQuery with $, even when in .noConflict() mode.
 *
 * Google CDN, Latest jQuery
 * To use the default WordPress version of jQuery, go to lib/config.php and
 * remove or comment out: add_theme_support('jquery-cdn');
 * ======================================================================== */

(function($) {
  

// Use this variable to set up the common and page specific functions. If you 
// rename this variable, you will also need to rename the namespace below.
var Roots = {

 // All pages
  common: {
    init: function() {

      // Sliding menu
      var padding= $('.stickyfooter').css('padding');

      $('button.navbar-toggle').click(function () {

          $('#wrapper').toggleClass('active');
          $(this).toggleClass('active');
          var target = $(this).attr('data-target');

          if ($('#wrapper').hasClass('active')) {

            $('header').css('left','0px');
            $('header button.navbar-toggle').css({'left':'10px', 'right':'initial'});
        //    $('.navbar-brand').addClass('hide');

            // Compensate for our stickyfooter being a bitch
            $('footer').addClass('hide');
            $('.stickyfooter').css({'padding':'0', 'height':'100%'});
            $('.st-menu').removeClass('invisible');

            // Hide elements and display only the one that is toggled
            $('button.navbar-toggle').addClass('hide');
            $(this).removeClass('hide');
            $('#sidebar-wrapper .mobile-menu').addClass('hide');
            $(target).removeClass('hide');


          } else {

            $('header').css('left','0px');
            $('header button.navbar-toggle').css({'right':'10px', 'left':'initial'});
            $('header button.navbar-toggle.toggle-two').css({'right':'58px', 'left':'initial'});

            $('.navbar-brand').removeClass('hide');

            // Turn our sticky footer back on
            $('footer').removeClass('hide')
            $('.stickyfooter').css('padding',padding);

            // Hide menu and show toggles
            $('.st-menu').addClass('invisible')
            $(target).addClass('hide');
            $('button.navbar-toggle').removeClass('hide');

            window.setTimeout(function() {   //need this timeout otherwise it shows up weird
                $('.stickyfooter').css('height','auto');
            },300);
          }

        });

        // mobile dropdown menu
        $('.navbar-mobile .dropdown:not(.on) .dropdown-toggle').click(function (event) {
            if ( ! $(this).parent().hasClass('on') ) {
                event.preventDefault();
                $(this).parent().toggleClass('on');
            }
        });

        $(window).resize(function() {
          if ( $(window).width() >= 769 ) {
            $('#wrapper').removeClass('active');
          }
        });
    },
    finalize: function() { }
  },
  // Home page
  home: {
    init: function() {
      // JavaScript to be fired on the home page
    }
  },
  // About us page, note the change from about-us to about_us.
  about_us: {
    init: function() {
      // JavaScript to be fired on the about us page
    }
  }
};

// The routing fires all common scripts, followed by the page specific scripts.
// Add additional events for more control over timing e.g. a finalize event
var UTIL = {
  fire: function(func, funcname, args) {
    var namespace = Roots;
    funcname = (funcname === undefined) ? 'init' : funcname;
    if (func !== '' && namespace[func] && typeof namespace[func][funcname] === 'function') {
      namespace[func][funcname](args);
    }
  },
  loadEvents: function() {
    UTIL.fire('common');

    $.each(document.body.className.replace(/-/g, '_').split(/\s+/),function(i,classnm) {
      UTIL.fire(classnm);
    });
  }
};

$(document).ready(UTIL.loadEvents);

$(document).ready(function() { // USED FOR MOTULITPLE THINGS

	$('.ginput_container').on('focusin', function(e,v){
		var $this = $(e.target),
			$parent = $this.closest('li');
			if( !$this.val() ){
				$($parent).addClass('gf-has-input');
			}
	});
	$('.ginput_container').on('focusout', function(e,v){
		var $this = $(e.target),
			$parent = $this.closest('li');
			if( !$this.val() ){
				$($parent).removeClass('gf-has-input');
			}
	});

	
	$('.toggle-gallery').on('click', function(){
//		$(this).css('visibility','hidden');
		$(this).find('span').text( ($(this).find('span').text() == 'View Gallery') ? 'Hide Gallery' :  'View Gallery' );
		$('.gallery-slider').toggle();
	});

	// home page view listings button
	$('.banner-button').on('click',function(){
		window.location = '/property';
	});
    //-----| Scroll to bottom on home page
	$('.diamond-line span').on('click', function(){
		$("body").animate({ scrollTop: $('#home-scroll').offset().top - 200 }, 1000);
	});

    //-----| Smooth Scroll to anchors
    $('a[href*=#]:not([href=#description], [href=#features], [href=#details])').click(function() {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top
                }, 1000);
                return false;
            }
        }
    });

    //--------| Animate our header on scroll
    $(window).scroll(function() {
        if ($(this).scrollTop() > 1){
          //  $('.navbar-fixed-top').addClass("sticky");
        }
        else{
        //    $('.navbar-fixed-top').removeClass("sticky");
        }
    });

    //-------| Move navbar to the top upon scroll option use navbar-static-top'
    // see http://www.bootply.com/107973
//    $('#nav').affix({
//        offset: {
//            top: $('header').height()
//        }
//    });


    //-------| Put text inside input boxes --------
    $('input[type="text"]').each(function() {
        $(this).attr("placeholder", $(this).attr("value")).removeAttr("value");
    });

    //-------| initiates "Slick.js" slider for property images --------
	$('.gallery-slider').slick({
		infinite: true,
		slidesToShow: 4,
		slidesToScroll: 1,
		prevArrow: '.nav-diamond.left',
		nextArrow: '.nav-diamond.right',
		responsive: [
		{
	      breakpoint: 768,
	      settings: "unslick"
	    }]
	});

    //-------| initiates popup lightbox images --------
	$('.popup-link').magnificPopup({ 
	  	type: 'image',
		callbacks: {
		  open: function() {
		    $('html').css('margin-right', 0); // hack to correct magnific margin addition
		  }
		}
	});

	$('.property-tabs a[href="#description"] , .property-tabs a[href="#features"] , .property-tabs a[href="#details"]').click(function (e) {
		e.preventDefault();
		var $id = $(this).attr('href');
		var $target = $( $id );
		$target.addClass('animated');
		$target.addClass('fadeIn');		
	})

    //-------| animations --------

	$gallery = $('.slick-active');
	$init = 0;
	$('.slick-slide').css('visibility', 'hidden');
	$.each($gallery, function(e,v){
		var $this = $(this);
		setTimeout( function(){
			$this.addClass('animated fadeInRight');
			$this.css('visibility', 'visible');
		}, $init );
		$init = $init + 1000;
	});
	setTimeout( function(){
		$('.slick-slide').css('visibility', 'visible');
	}, $gallery.length * 1000 );

	//--------| Property Plugin is dynamically putting empty anchors it seems
	$('a').each( function(){
		if( $(this).html().length == 0 ){
			$(this).remove();
		}
	});

    //-------| property search / modifies & customizes the easypropertylistings search form  --------
	var $fields = {
		'property_bathrooms' : 'BATH',
		'property_bedrooms_min' : 'MIN BEDS',
		'property_bedrooms_max' : 'MAX BEDS',
		'property_building_area_min' : 'MIN SQFT',
		'property_building_area_max' : 'MAX SQFT',
		'property_building_area_unit' : 'units',
		'property_location' : 'LOCATION',
		'property_price_from' : 'MIN PRICE',
		'property_price_to' : 'MAX PRICE'		
	};

	var $form_elements = {};
	$('.epl-search-row, .epl-search-row-full, .epl-search-row-half, .epl-search-row-third').each( function(){
		var $this = $($(this)[0]),
			$label = $($($this.find('label'))[0]),
			$field = $($($this.find('.field'))[0]),
			$type = $label.attr('for');
			if( $field.length ){
				if( $type == 'property_location' ){
					var $div = $("<div>", {class: "gf_field col-md-3 col-sm-6 col-xs-12 property_location"});
					var $label = $("<label class='hidden-lg hidden-xs'>");
					$label.html( $fields[$type] );
					$div.append( $label);	
				}
				else if( $type == 'property_building_area_unit' ){
					var $div = $("<div>", {class: "area_unit"});
				}
				else{
					var $div = $("<div>", {class: "gf_field col-md-3 col-sm-6 col-xs-12"});
					var $label = $("<label>");
					$label.html( $fields[$type] );
					$div.append( $label);	
				}
				$div.append( $field);
				var $select = $field.children('select'); 
				$($select).addClass('cs-select');
				if( $select.length && 'undefined' != typeof $fields[$type] ){
					var $options = $(($select)[0] ).children('option');
					if( $options.length ){
						$.each($options, function(e,v){
							$(this).attr('dir','rtl');
							if( !$(v).val() ){
								if( $type == 'property_location' ){
									$(v).html('<span class="hidden-md hidden-sm">LOCATION</span><span class="hidden-lg hidden-xs">Any</span>');
								}
								else{
									$(v).html('');
								}
							}
						}); 
if( $type != 'property_location' ){
	var $anyOption = $("<option>").html('Any');
}
$($select).prepend( $anyOption);

					}
				}
		
				$form_elements[$type] = $div;
				$this.remove();
			}
		});
		// create new form
		var $form_display = $("<div>", {class: "gf_form"});
		var $row = $("<div>", {class: "gf_form_row row"});
		$row.append(	$form_elements.property_location );
		$row.append(	$form_elements.property_bedrooms_min );
		$row.append(	$form_elements.property_building_area_min );
		$row.append(	$form_elements.property_price_from );
		$form_display.append ( $row );
		var $row = $("<div>", {class: "gf_form_row row"});
		$row.append(	$form_elements.property_bathrooms );
		$row.append(	$form_elements.property_bedrooms_max );
		$row.append(	$form_elements.property_building_area_max );
		$row.append(	$form_elements.property_price_to);
		$form_display.append(	$row );
		$form_display.append(	$form_elements.property_building_area_unit);
		var $container = $('.epl-search-form form');
		$($container[0]).prepend( $form_display );	
		$('#property_building_area_unit').val( 'sqft' );

		$container.find('select').addClass('input-block-level');
		var $submit = $('.epl-search-submit-row'),
			$html = $($submit).html();
			$($submit).remove();
			$('.submit-search').html("<div class='row'><button type='button' id='submit_property_search'>Search</button></div>");

		//$('#property_location').parent().css('width','100%').css('float','none');
		//$('#property_location').css('width','100%');
		$('.epl-search-row select').on('click', function(){
			var $this = $(this);
			var $label = $this.parent().siblings('label').hide();
		});
		$('#submit_property_search').on('click', function(){
			$('.epl-search-form form').submit();
		});
		$('#search-row').removeClass('hidden-search');

		$(window).scroll(function () {	
			var scrollTop     = $(window).scrollTop(),
	    		elementOffset = $('#page-name-banner').offset().top,
	    		distance      = (elementOffset - scrollTop),
				$width = $(window).width();
			if( $width > 767 ){
			    if ( distance < -25 ) {
					$('#page-name-banner h1').hide();
				}
				else{
					$('#page-name-banner h1').fadeIn();
				}
			}
			if( $width > 991 ){
				if ( distance < -96  ) {
					$('.navbar-default').addClass('fixed-background');
				}
				else{
					$('.navbar-default').removeClass('fixed-background');
				}
			}
			else{
				if ( distance < -45  ) {
					$('.navbar-default').addClass('fixed-background');
				}
				else{
					$('.navbar-default').removeClass('fixed-background');
					$('#menu-primary-navigation').css('margin-top', (45 + distance) + 'px');	
				}
			}
		});
/**
 * selectFx.js v1.0.0
 * http://www.codrops.com
 *
 * Licensed under the MIT license.
 * http://www.opensource.org/licenses/mit-license.php
 * 
 * Copyright 2014, Codrops
 * http://www.codrops.com
 */
;( function( window ) {
	
	'use strict';

	/**
	 * based on from https://github.com/inuyaksa/jquery.nicescroll/blob/master/jquery.nicescroll.js
	 */
	function hasParent( e, p ) {
		if (!e) return false;
		var el = e.target||e.srcElement||e||false;
		while (el && el != p) {
			el = el.parentNode||false;
		}
		return (el!==false);
	};
	
	/**
	 * extend obj function
	 */
	function extend( a, b ) {
		for( var key in b ) { 
			if( b.hasOwnProperty( key ) ) {
				a[key] = b[key];
			}
		}
		return a;
	}

	/**
	 * SelectFx function
	 */
	function SelectFx( el, options ) {	
		this.el = el;
		this.options = extend( {}, this.options );
		extend( this.options, options );
		this._init();
	}

	/**
	 * SelectFx options
	 */
	SelectFx.prototype.options = {
		// if true all the links will open in a new tab.
		// if we want to be redirected when we click an option, we need to define a data-link attr on the option of the native select element
		newTab : true,
		// when opening the select element, the default placeholder (if any) is shown
		stickyPlaceholder : true,
		// callback when changing the value
		onChange : function( val ) { return false; }
	}

	/**
	 * init function
	 * initialize and cache some vars
	 */
	SelectFx.prototype._init = function() {
		// check if we are using a placeholder for the native select box
		// we assume the placeholder is disabled and selected by default
		var selectedOpt = this.el.querySelector( 'option[selected]' );
		this.hasDefaultPlaceholder = selectedOpt && selectedOpt.disabled;

		// get selected option (either the first option with attr selected or just the first option)
		this.selectedOpt = selectedOpt || this.el.querySelector( 'option' );

		// create structure
		this._createSelectEl();

		// all options
		this.selOpts = [].slice.call( this.selEl.querySelectorAll( 'li[data-option]' ) );
		
		// total options
		this.selOptsCount = this.selOpts.length;
		
		// current index
		this.current = this.selOpts.indexOf( this.selEl.querySelector( 'li.cs-selected' ) ) || -1;
		
		// placeholder elem
		this.selPlaceholder = this.selEl.querySelector( 'span.cs-placeholder' );

		// init events
		this._initEvents();
	}

	/**
	 * creates the structure for the select element
	 */
	SelectFx.prototype._createSelectEl = function() {
		var self = this, options = '', createOptionHTML = function(el) {
			var optclass = '', classes = '', link = '';

			if( el.selectedOpt && !this.foundSelected && !this.hasDefaultPlaceholder ) {
				classes += 'cs-selected ';
				this.foundSelected = true;
			}
			// extra classes
			if( el.getAttribute( 'data-class' ) ) {
				classes += el.getAttribute( 'data-class' );
			}
			// link options
			if( el.getAttribute( 'data-link' ) ) {
				link = 'data-link=' + el.getAttribute( 'data-link' );
			}

			if( classes !== '' ) {
				optclass = 'class="' + classes + '" ';
			}

			return '<li ' + optclass + link + ' data-option data-value="' + el.value + '"><span>' + el.textContent + '</span></li>';
		};

		[].slice.call( this.el.children ).forEach( function(el) {

			if( el.disabled || el.value == '') { return; }

			var tag = el.tagName.toLowerCase();

			if( tag === 'option' ) {
				options += createOptionHTML(el);
			}
			else if( tag === 'optgroup' ) {
				options += '<li class="cs-optgroup"><span>' + el.label + '</span><ul>';
				[].slice.call( el.children ).forEach( function(opt) {
					options += createOptionHTML(opt);
				} )
				options += '</ul></li>';
			}
		} );

		var opts_el = '<div class="cs-options grey-back"><ul>' + options + '</ul></div>';
		this.selEl = document.createElement( 'div' );
		this.selEl.className = this.el.className;
		this.selEl.tabIndex = this.el.tabIndex;
		this.selEl.innerHTML = '<span class="cs-placeholder">' + this.selectedOpt.innerHTML + '</span>' + opts_el;
		this.el.parentNode.appendChild( this.selEl );
		this.selEl.appendChild( this.el );
	}

	/**
	 * initialize the events
	 */
	SelectFx.prototype._initEvents = function() {
		var self = this;

		// open/close select
		this.selPlaceholder.addEventListener( 'click', function() {
			self._toggleSelect();
		} );

		// clicking the options
		this.selOpts.forEach( function(opt, idx) {
			opt.addEventListener( 'click', function() {
				self.current = idx;
				self._changeOption();
				// close select elem
				self._toggleSelect();
			} );
		} );

		// close the select element if the target it´s not the select element or one of its descendants..
		document.addEventListener( 'click', function(ev) {
			var target = ev.target;
			if( self._isOpen() && target !== self.selEl && !hasParent( target, self.selEl ) ) {
				self._toggleSelect();
			}
		} );

		// keyboard navigation events
		this.selEl.addEventListener( 'keydown', function( ev ) {
			var keyCode = ev.keyCode || ev.which;

			switch (keyCode) {
				// up key
				case 38:
					ev.preventDefault();
					self._navigateOpts('prev');
					break;
				// down key
				case 40:
					ev.preventDefault();
					self._navigateOpts('next');
					break;
				// space key
				case 32:
					ev.preventDefault();
					if( self._isOpen() && typeof self.preSelCurrent != 'undefined' && self.preSelCurrent !== -1 ) {
						self._changeOption();
					}
					self._toggleSelect();
					break;
				// enter key
				case 13:
					ev.preventDefault();
					if( self._isOpen() && typeof self.preSelCurrent != 'undefined' && self.preSelCurrent !== -1 ) {
						self._changeOption();
						self._toggleSelect();
					}
					break;
				// esc key
				case 27:
					ev.preventDefault();
					if( self._isOpen() ) {
						self._toggleSelect();
					}
					break;
			}
		} );
	}

	/**
	 * navigate with up/dpwn keys
	 */
	SelectFx.prototype._navigateOpts = function(dir) {
		if( !this._isOpen() ) {
			this._toggleSelect();
		}

		var tmpcurrent = typeof this.preSelCurrent != 'undefined' && this.preSelCurrent !== -1 ? this.preSelCurrent : this.current;
		
		if( dir === 'prev' && tmpcurrent > 0 || dir === 'next' && tmpcurrent < this.selOptsCount - 1 ) {
			// save pre selected current - if we click on option, or press enter, or press space this is going to be the index of the current option
			this.preSelCurrent = dir === 'next' ? tmpcurrent + 1 : tmpcurrent - 1;
			// remove focus class if any..
			this._removeFocus();
			// add class focus - track which option we are navigating
			classie.add( this.selOpts[this.preSelCurrent], 'cs-focus' );
		}
	}

	/**
	 * open/close select
	 * when opened show the default placeholder if any
	 */
	SelectFx.prototype._toggleSelect = function() {
		// remove focus class if any..
		this._removeFocus();
		if( this._isOpen() ) {
			if( this.current !== -1 ) {
				// update placeholder text
				this.selPlaceholder.textContent = this.selOpts[ this.current ].textContent;
			}
			classie.remove( this.selEl, 'cs-active' );
		}
		else {
			if( this.hasDefaultPlaceholder && this.options.stickyPlaceholder ) {
				// everytime we open we wanna see the default placeholder text
				this.selPlaceholder.textContent = this.selectedOpt.textContent;
			}
			classie.add( this.selEl, 'cs-active' );
		}
	}

	/**
	 * change option - the new value is set
	 */
	SelectFx.prototype._changeOption = function() {
		// if pre selected current (if we navigate with the keyboard)...
		if( typeof this.preSelCurrent != 'undefined' && this.preSelCurrent !== -1 ) {
			this.current = this.preSelCurrent;
			this.preSelCurrent = -1;
		}

		// current option
		var opt = this.selOpts[ this.current ];

		// update current selected value
		this.selPlaceholder.textContent = opt.textContent;
		
		// change native select element´s value
		this.el.value = opt.getAttribute( 'data-value' );

		// remove class cs-selected from old selected option and add it to current selected option
		var oldOpt = this.selEl.querySelector( 'li.cs-selected' );
		if( oldOpt ) {
			classie.remove( oldOpt, 'cs-selected' );
		}
		classie.add( opt, 'cs-selected' );

		// if there´s a link defined
		if( opt.getAttribute( 'data-link' ) ) {
			// open in new tab?
			if( this.options.newTab ) {
				window.open( opt.getAttribute( 'data-link' ), '_blank' );
			}
			else {
				window.location = opt.getAttribute( 'data-link' );
			}
		}

		// callback
		this.options.onChange( this.el.value );
	}

	/**
	 * returns true if select element is opened
	 */
	SelectFx.prototype._isOpen = function(opt) {
		return classie.has( this.selEl, 'cs-active' );
	}

	/**
	 * removes the focus class from the option
	 */
	SelectFx.prototype._removeFocus = function(opt) {
		var focusEl = this.selEl.querySelector( 'li.cs-focus' )
		if( focusEl ) {
			classie.remove( focusEl, 'cs-focus' );
		}
	}

	/**
	 * add to global namespace
	 */
	window.SelectFx = SelectFx;

} )( window );

			(function() {
				[].slice.call( document.querySelectorAll( '.field select' ) ).forEach( function(el) {	
					new SelectFx(el);
				} );
			})();
			// adjust cell widths to grandparent;
			var $dropdown_containers = $('.cs-options');
			$.each( $dropdown_containers , function ( e,v ){
				var $width = $(v).closest('.gf_field').width();
				$(v).width( $width );
			})
});
	// gform effects
	
})(jQuery); // Fully reference jQuery after this point.
