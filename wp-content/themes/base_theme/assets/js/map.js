;jQuery(document).ready(function($) {
	if( $('#map-canvas').length){
		function initialize() {

			var mapLat = markers[0][1];
			var mapLng = markers[0][2];
			var mapCenter = new google.maps.LatLng(mapLat,mapLng);
			var mapOptions = {
				zoom: 15,
				center: mapCenter,
//				mapTypeId: google.maps.MapTypeId.HYBRID,
				overviewMapControl: false,
				panControl: true,
				draggable: false,
				zoomControl: true,
				disableDefaultUI: true,
			}
		
			map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
	
		    var infoWindow = new google.maps.InfoWindow(), marker, i;
	
			// build a global array for accessing all the markers globally
	  		allMarkers = [];
	
			for( i = 0; i < markers.length; i++ ) {
		        var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
	
		        marker = new google.maps.Marker({
		            position: position,
		            map: map,
		            title: markers[i][0],
		        });
		        marker.set("type", markers[i][3]);
				allMarkers.push(marker);
	
		        // Allow each marker to have an info window    
		        google.maps.event.addListener(marker, 'click', (function( marker, i) {
			        return function() {
			            /* setup ajax loader for marker descriptions */
		                infoWindow.setContent(infoWindowContent[i][0]);
		                infoWindow.open(map, marker);
		            }
		        })(marker, i));	
	
		    }
	
			// STYLE MAP		
			var styles = [{"featureType":"administrative","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"administrative","elementType":"geometry","stylers":[{"visibility":"simplified"}]},{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#444444"}]},{"featureType":"landscape.man_made","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"landscape.man_made","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"hue":"#ffb300"},{"saturation":"32"},{"lightness":"0"}]},{"featureType":"landscape.natural","elementType":"all","stylers":[{"visibility":"simplified"},{"hue":"#ffb800"},{"saturation":"54"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":-100},{"lightness":45},{"visibility":"on"}]},{"featureType":"road","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"hue":"#ffb800"},{"saturation":"29"},{"lightness":"64"},{"gamma":"1"}]},{"featureType":"road","elementType":"geometry.stroke","stylers":[{"color":"#ffda7b"}]},{"featureType":"road","elementType":"labels","stylers":[{"visibility":"simplified"}]},{"featureType":"road","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"visibility":"simplified"}]},{"featureType":"road.highway","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"road.highway","elementType":"labels.text.fill","stylers":[{"color":"#ffda7b"}]},{"featureType":"road.highway","elementType":"labels.text.stroke","stylers":[{"color":"#ffda7b"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"transit","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"saturation":"-60"},{"color":"#d5cdb6"}]}];
			map.setOptions({styles: styles});
		}
			
		google.maps.event.addDomListener(window, 'load', initialize);
	
	}
});