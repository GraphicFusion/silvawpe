<?php if( !is_home() && !is_front_page() ){ $smallFooter = false; $class = ' tan-break'; }else{ $smallFooter = true; $class='home'; } ?>

<footer class="content-info footer black-back <?php echo $class; ?>" role="contentinfo">

	<div class="container wrap">
		<?php if( !$smallFooter || 1==1 ) : ?>
			<nav class="collapse navbar-collapse" role="navigation">
		      <?php get_template_part('templates/content', 'nav-diamonds'); ?>
			</nav>
		<?php endif; ?>
	
	</div>
		<p class="copyright">&copy; <?php echo date('Y'); ?> <?php echo strtoupper( get_bloginfo( 'name' ) ); ?> | <a href='https://graphicfusiondesign.com/'>DESIGNED BY GRAPHIC FUSION DESIGN</a></p>

</footer>
