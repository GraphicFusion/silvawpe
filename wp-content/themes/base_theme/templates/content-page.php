<?php if (have_posts()) : while (have_posts()) : the_post(); global $post; ?>

		<div class='container general-page'>
			<div class='row'>
				<div class="col-md-12">	
					<h1><?php the_title( ); ?></h1>
					<?php the_content( ); ?>
				</div>
			</div>
		</div>
	
<?php endwhile; endif; ?>
