<header class="banner navbar navbar-default navbar-fixed-top" role="banner" id="header">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
				<span class="sr-only"><?= __('Toggle navigation', 'sage'); ?></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				</button>
				<img style="max-height:68px;" src="<?php echo get_template_directory_uri() ."/assets/img/experience_sothebys.png"; ?>">
				<a class="navbar-brand" href="<?= esc_url(home_url('/')); ?>">
					JOSHUA SHARON
				</a>
			</div>
			<nav class="collapse navbar-collapse" role="navigation">
				<?php
				if (has_nav_menu('primary_navigation')) :
				wp_nav_menu( array( 'theme_location' => 'primary_navigation', 'walker' => new Roots_Nav_Walker(), 'menu_class' => 'nav navbar-nav' ) );
				endif;
				?>
			</nav>
	</header>