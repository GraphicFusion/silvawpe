<?php
/*
Template Name: About
*/
?>
<div class='about-section'>
	<div class='container'>
		<div class='row'>
			<div class='col-md-7 right-border'>
				<div class='row'>
					<div class='col-md-12'>
						<div class='about-name'>JOSHUA SHARON</div>
						<h2>About</h2>
						<div class='info-about'>
							<?php the_field('info-about', 'option'); ?>			
						</div>
					</div>	
					<div class='col-md-12'>
						<div class='info-experience'>
							<h2>Experience</h2>
							<?php the_field('info-experience', 'option'); ?>			
						</div>
					</div>	
				</div>
			</div>
			<div class='col-md-5'>
				<h2>Contact</h2>
				<div class='row info-contact'>
					<div class='col-sm-6 col-md-4 col-lg-12'>
					<?php $image = get_field('info-image', 'option'); ?>
					<img src="<?php echo $image['sizes']['thumbnail']; ?>">			
					</div>
					<div class='col-sm-6 col-md-8 col-lg-12'>
						<div class='row'>
							<div class='col-md-12'>
								<label>phone</label>
								<span><?php the_field('info-phone', 'option'); ?></span>
							</div>			
							<div class='col-md-12'>
								<label>email</label>
								<span><?php the_field('info-email', 'option'); ?></span>
							</div>
						</div>
					</div>			
				</div>
				<?php if( have_rows('info-awards', 'option') ): ?>
					<div class='awards-section'>
						<h2>Awards</h2>
				
					    <?php while( have_rows('info-awards', 'option') ): the_row(); ?>
				
							<div class='award'>
								<label class='copper chron'><?php the_sub_field('award-name'); ?></label>
								<label><?php the_sub_field('award-grantee'); ?></label>
								<p><?php the_sub_field('award-description'); ?></p>
							</div>
				
					    <?php endwhile; ?>
				
					</div><!--/awards-section-->
				    
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>
<?php if( have_rows('testimonials', 'option') ): ?>
	<div class='testimonials-section black-back'>
		<div class='container'>
			<h2>Testimonials</h2>
			<div class='row'>

			    <?php while( have_rows('testimonials', 'option') ): the_row(); ?>
		
					<div class='col-md-4 testimonial'>
						<label><?php the_sub_field('name'); ?></label>
						<p><?php the_sub_field('quote'); ?></p>
					</div>
		
			    <?php endwhile; ?>

			</div><!--/row-->
		</div><!--/container-->
	</div><!--/testimonials-section-->
    
<?php endif; ?>
<?php
	$args = array(
	    'numberposts' => 3,
	    'orderby' => 'post_date',
	    'order' => 'DESC',
	    'post_type' => 'post',
	    'post_status' => 'publish' );

    $recent_posts = wp_get_recent_posts( $args, ARRAY_A );
	if( count( $recent_posts ) > 0 ) : $temp = $post; ?>
<div class='blog-section'>
	<div class='container'>
		<h2>News and Blog</h2>
		<div class='row'>
			<?php foreach( $recent_posts as $r_post ) :  $post = get_post( $r_post['ID']); setup_postdata( $post ); ?>

				<div class='col-md-4'>
					<div class='recent-post black-back'>
						<label><?php the_title(); ?></label>
						<hr>
						<span class='date'><label>Date</label><?php echo get_the_date( 'd/m/Y' , $post->ID );  ?></span>
						<?php the_excerpt( ); ?>
					</div>
				</div>

			<?php wp_reset_postdata(); endforeach; ?>
		</div>
	</div>
</div>
<?php $post = $temp; endif; ?>