<nav role="navigation">
	<?php
	if (has_nav_menu('primary_navigation')) :
	wp_nav_menu( array( 'theme_location' => 'primary_navigation', 'walker' => new Roots_Nav_Walker(), 'menu_class' => 'nav navbar-nav' ) );
	endif;
	?>
</nav>