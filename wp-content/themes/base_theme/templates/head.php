<!doctype html>
<html class="no-js" <?php language_attributes(); ?>>
	<script>
		// not using modernizr:
		var $html = document.getElementsByTagName('html')[0];
		$html.className = $html.className.replace("no-js","");
	</script>
	<head>
		<title><?php wp_title('|',true,'right'); ?></title>
		<meta charset="utf-8">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<?php wp_head(); ?>
	</head>