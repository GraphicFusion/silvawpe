<?php
/*
Template Name: Contact
*/
?>
<div class='about-section'>
	<div class='container'>
		<div class='row'>
			<div class='col-md-7 right-border'>
				<?php if(have_posts()) : ?>
					<h1>Get in touch</h1>
					<?php while(have_posts()) : the_post(); ?>								
						<?php the_content(); ?>
					<?php endwhile; ?>
				<?php endif; ?>	
			</div>
			<div class='col-md-5 contact-info'>
				<h1>Contact</h1>
				<div class='col-md-12'>
					<label>Name</label>
					<span><?php the_field('info-name', 'option'); ?></span>
				</div>
				<div class='col-md-12'>
					<label>Hours</label>
					<span><?php the_field('info-hours', 'option'); ?></span>
				</div>
				<div class='col-md-12'>
					<label>Email</label>
					<span><?php the_field('info-email', 'option'); ?></span>
				</div>
				<div class='col-md-12'>
					<label>Phone</label>
					<span><?php the_field('info-phone', 'option'); ?></span>
				</div>
				<div class='col-md-12'>
					<label>Fax</label>
					<span><?php the_field('info-fax', 'option'); ?></span>
				</div>
			</div>
		</div>
	</div>
</div>