<?php
/*
Template Name: Front page
*/
?>
    <div class='home-banner black-back'>
		<div id='banner-image'>
			<div class='banner-footer tan-break black-back'>
				<div class='diamond-line'>
						<div class='small-d-wrap'>
							<div class='small-diamond'></div>
							<div class='small-diamond'></div>
							<div class='small-diamond'></div>
							<div class='small-diamond'></div>
						</div>
						<span>MORE INFO</SPAN>
				</div>
			</div>
		</div>
		<div id='banner-teaser'>
			<div>
				<span class='copper'>FIND YOUR</span>
				<span class='chron'>DREAM HOME</span>
				<span class='banner-button'>View Listings</span>
			</div>	
		</div>
	</div>
	<div class="home-section black-back" id='home-section'>
		<div class='row'>
			<div class='col-md-8 col-md-offset-2' id='home-scroll'>
				<h1><?php echo get_field('more-info-title'); ?></h1>
				<div class='row'>
					<div class='col-md-8 col-md-offset-2'>
						<p><?php echo get_field('more-info-text'); ?></p>
					</div>
				</div>
			</div>
		</div>
	</div>	
	<div class='clearfix'></div>