<?php get_template_part('templates/head'); ?>
<?php

	/**
	 *	This theme adds a block to the wrapper that mimics the navbar; pushing it up and dissapearing  
	 *	The following checks if current page is a property archive OR has an acf page name;
	 *	if so, the class "page-named" is added to the body
	 */ 
	if( 'Properties' == trim( post_type_archive_title('', false) ) ){
		// custom H1 Page Name for archives of EasyPropertyListings plugin's default CPT properties
		$page_name = 'View Listings';
	}
	else{
		// gets page's acf field
		$page_name =  get_field('page-name');
	}
	$body_class = "";
	if( $page_name ){ 
		$body_class = "page-named";
	}
?>

<body <?php body_class( $body_class ); ?>>

	<!--[if lt IE 8]>
		<div class="alert alert-warning">
			<?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'roots'); ?>
		</div>
	<![endif]-->

    <!--[if lte IE 8]>
	    <script type="text/javascript" src="https://cdn.jsdelivr.net/modernizr/2.8.3/modernizr.min.js"></script>
	    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.js"></script>
    <![endif]-->
	<div id="wrapper" class="stickyfooter st-container st-effect-7">
        <div class="st-pusher">
            <?php
            	do_action('get_header');
				get_template_part('templates/header-top-navbar');
            ?>

			 <div id="sidebar-wrapper" class="sidebar-wrapper navbar-mobile st-menu st-effect-7 hidden-lg hidden-md hidden-sm" role="navigation">
                <?php get_template_part('templates/mobile', 'nav'); ?>
			 </div>

			<div id="content-wrapper" class="st-content">
				<div id='page-name-banner'>
					<?php if( $page_name ) : ?>
						<h1><?php echo strtoupper( $page_name ); ?></h1>
					<?php endif; ?>
				</div>
				<script>
					var w = window,
					    d = document,
						e = d.documentElement,
						g = d.getElementsByTagName('body')[0],
						h = d.getElementsByTagName('header')[0],
						hY = h.offsetHeight,
						Y = w.innerHeight|| e.clientHeight|| g.clientHeight,
							bannerHeight = Y - hY;
						//d.getElementById('page-name-banner').setAttribute("style","padding-top:" + hY + "px" );
				</script>
				<div class="wrap <?php // echo get_roots_theme_size(); ?> st-content-inner" role="document">
					<div class="content row">
						<main id="main" class="main <?php echo roots_main_class(); ?>" role="main">
							<?php include roots_template_path(); ?>
						</main><!-- /.main -->

						<?php if (roots_display_sidebar()) : ?>
							<aside class="sidebar <?php echo roots_sidebar_class(); ?>" role="complementary">
								<?php include roots_sidebar_path(); ?>
							</aside><!-- /.sidebar -->
						<?php endif; ?>
					</div><!-- /.content -->
				</div><!-- /.wrap -->
			</div><!-- /.content-wrapper -->
		</div><!-- /.wrapper -->
	</div><!-- /.stickyfooter -->
	<?php get_template_part('templates/footer'); ?>


</body>
</html>
