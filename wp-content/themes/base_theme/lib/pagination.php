<?php
/**
 * Pagination option // forked from easypropertylistings plugins by WJGRASS
 *
 * @package     EPL
 * @subpackage  Pagination
 * @copyright   Copyright (c) 2014, Merv Barrett
 * @license     http://opensource.org/licenses/gpl-2.0.php GNU Public License
 * @since       2.1
*/

function gf_pagination( $args = array() ) {
	if ( !is_array( $args ) ) {
		$argv = func_get_args();

		$args = array();
		foreach ( array( 'before', 'after', 'options' ) as $i => $key )
			$args[ $key ] = isset( $argv[ $i ]) ? $argv[ $i ] : "";
	}

	$args = wp_parse_args( $args, array(
		'before' => '',
		'after' => '',
		'options' => array(),
		'query' => $GLOBALS['wp_query'],
		'type' => 'posts',
		'echo' => true
	) );

	extract( $args, EXTR_SKIP );
	$options = array(
		'current_text'  => '%PAGE_NUMBER%',
		'page_text'     => '%PAGE_NUMBER%',
		'first_text'    => __( '&laquo; First', 'epl' ),
		'last_text'     => __( 'Last &raquo;', 'epl' ),
		'prev_text'     => __( 'PREV', 'epl' ),
		'next_text'     => __( 'NEXT', 'epl' ),
		'dotleft_text'  => __( '...', 'epl' ),
		'dotright_text' => __( '...', 'epl' ),
		'num_pages' => 20,
		'num_larger_page_numbers' => 3,
		'larger_page_numbers_multiple' => 10,
		'always_show' => false,
		'use_pagenavi_css' => true
	);
	$options = apply_filters('gf_pagination_options',$options);
	$instance = new gf_pagination_Call( $args );

	list( $posts_per_page, $paged, $total_pages ) = $instance->get_pagination_args();

	if ( 1 == $total_pages && !$options['always_show'] )
		return;

	$pages_to_show = absint( $options['num_pages'] );
	$larger_page_to_show = absint( $options['num_larger_page_numbers'] );
	$larger_page_multiple = absint( $options['larger_page_numbers_multiple'] );
	$pages_to_show_minus_1 = $pages_to_show - 1;
	$half_page_start = floor( $pages_to_show_minus_1/2 );
	$half_page_end = ceil( $pages_to_show_minus_1/2 );
	$start_page = $paged - $half_page_start;

	if ( $start_page <= 0 )
		$start_page = 1;

	$end_page = $paged + $half_page_end;

	if ( ( $end_page - $start_page ) != $pages_to_show_minus_1 )
		$end_page = $start_page + $pages_to_show_minus_1;

	if ( $end_page > $total_pages ) {
		$start_page = $total_pages - $pages_to_show_minus_1;
		$end_page = $total_pages;
	}

	if ( $start_page < 1 )
		$start_page = 1;

	$out = '';

		// Previous
		if ( $paged > 1 && !empty( $options['prev_text'] ) ) {
			$prev = $instance->get_single( $paged - 1, $options['prev_text'], array(
				'class' => 'previouspostslink',
				'rel'	=> 'prev'
			) );
		}
		else{
			$prev = "<a>PREV</a>";
		}

		if ( $start_page >= 2 && $pages_to_show < $total_pages ) {
			if ( !empty( $options['dotleft_text'] ) )
				$out .= "<span class='extend'>{$options['dotleft_text']}</span>";
		}

		// Smaller pages
		$larger_pages_array = array();
		if ( $larger_page_multiple )
			for ( $i = $larger_page_multiple; $i <= $total_pages; $i+= $larger_page_multiple )
				$larger_pages_array[] = $i;

		$larger_page_start = 0;
		foreach ( $larger_pages_array as $larger_page ) {
			if ( $larger_page < ($start_page - $half_page_start) && $larger_page_start < $larger_page_to_show ) {
				$out .= $instance->get_single( $larger_page, $options['page_text'], array(
					'class' => 'smaller page',
				) );
				$larger_page_start++;
			}
		}

		if ( $larger_page_start )
			$out .= "<span class='extend'>{$options['dotleft_text']}</span>";

		// Page numbers
		$timeline = 'smaller';
		foreach ( range( $start_page, $end_page ) as $i ) {
			if ( $i == $paged && !empty( $options['current_text'] ) ) {
				$current_page_text = str_replace( '%PAGE_NUMBER%', number_format_i18n( $i ), $options['current_text'] );
				$out .= "<div class='small-diamond current'>$current_page_text</div><span></span>";
				$timeline = 'larger';
			} else {
				$out .= $instance->get_single( $i, $options['page_text'], array(
					'class' => "page $timeline",
				) );
			}
		}

		// Large pages
		$larger_page_end = 0;
		$larger_page_out = '';
		foreach ( $larger_pages_array as $larger_page ) {
			if ( $larger_page > ($end_page + $half_page_end) && $larger_page_end < $larger_page_to_show ) {
				$larger_page_out .= $instance->get_single( $larger_page, $options['page_text'], array(
					'class' => 'larger page',
				) );
				$larger_page_end++;
			}
		}

		if ( $larger_page_out ) {
			$out .= "<span class='extend'>{$options['dotright_text']}</span>";
		}
		$out .= $larger_page_out;

		if ( $end_page < $total_pages ) {
			if ( !empty( $options['dotright_text'] ) )
				$out .= "<span class='extend'>{$options['dotright_text']}</span>";
		}

		// Next
		if ( $paged < $total_pages && !empty( $options['next_text'] ) ) {
			$next = $instance->get_single( $paged + 1, $options['next_text'], array(
				'class' => 'nextpostslink',
				'rel'	=> 'next'
			) );
		}
		else{
			$next = "<a>NEXT</a>";
		}

		if ( $end_page < $total_pages ) {
			// Last
			$out .= $instance->get_single( $total_pages, $options['last_text'], array(
				'class' => 'last',
			), '%TOTAL_PAGES%' );
		}
	$out = "<div class='small-d-wrap'>".$prev.$out.$next."</div>";
	$out = apply_filters( 'gf_pagination_html', $out );
	
	if ( !$echo )
		return $out;

	echo $out;
}


class gf_pagination_Call {

	protected $args;

	function __construct( $args ) {
		$this->args = $args;
	}

	function __get( $key ) {
		return $this->args[ $key ];
	}

	function get_pagination_args() {
		global $numpages;

		$query = $this->query;

		switch( $this->type ) {
		case 'multipart':
			// Multipart page
			$posts_per_page = 1;
			$paged = max( 1, absint( get_query_var( 'page' ) ) );
			$total_pages = max( 1, $numpages );
			break;
		case 'users':
			// WP_User_Query
			$posts_per_page = $query->query_vars['number'];
			$paged = max( 1, floor( $query->query_vars['offset'] / $posts_per_page ) + 1 );
			$total_pages = max( 1, ceil( $query->total_users / $posts_per_page ) );
			break;
		default:
			// WP_Query
			$posts_per_page = intval( $query->get( 'posts_per_page' ) );
			$paged = max( 1, absint( $query->get( 'paged' ) ) );
			$total_pages = max( 1, absint( $query->max_num_pages ) );
			break;
		}

		return array( $posts_per_page, $paged, $total_pages );
	}

	function get_single( $page, $raw_text, $attr, $format = '%PAGE_NUMBER%' ) {
		if ( empty( $raw_text ) )
			return '';

		$text = str_replace( $format, number_format_i18n( $page ), $raw_text );

		$attr['href'] = $this->get_url( $page );
		if ( !array_key_exists( 'rel', $attr ) || ( $attr['rel'] != 'next' && $attr['rel'] != 'prev' ) ){
				$anchor = "<div class='small-diamond'>".gf_pagination_html( 'a', $attr, $text )."</div><span></span>";
			
		}
		else{
			$anchor = gf_pagination_html( 'a', $attr, $text );
		}
		return $anchor;
	}

	function get_url( $page ) {
		return ( 'multipart' == $this->type ) ? get_multipage_link( $page ) : get_pagenum_link( $page );
	}
}

if ( ! function_exists( 'gf_pagination_html' ) ):
function gf_pagination_html( $tag ) {

	static $SELF_CLOSING_TAGS = array( 'area', 'base', 'basefont', 'br', 'hr', 'input', 'img', 'link', 'meta' );

	$args = func_get_args();

	$tag = array_shift( $args );

	if ( is_array( $args[0] ) ) {
		$closing = $tag;
		$attributes = array_shift( $args );
		foreach ( $attributes as $key => $value ) {
			if ( false === $value )
				continue;

			if ( true === $value )
				$value = $key;

			$tag .= ' ' . $key . '="' . esc_attr( $value ) . '"';
		}
	} else {
		list( $closing ) = explode( ' ', $tag, 2 );
	}

	if ( in_array( $closing, $SELF_CLOSING_TAGS ) ) {
		return "<{$tag} />";
	}

	$content = implode( '', $args );

	return "<{$tag}>{$content}</{$closing}>";
}
endif;

if ( !function_exists( 'gf_get_multipage_link' ) ) :
function gf_get_multipage_link( $page = 1 ) {
	global $post, $wp_rewrite;

	if ( 1 == $page ) {
		$url = get_permalink();
	} else {
		if ( '' == get_option('permalink_structure') || in_array( $post->post_status, array( 'draft', 'pending') ) )
			$url = add_query_arg( 'page', $page, get_permalink() );
		elseif ( 'page' == get_option( 'show_on_front' ) && get_option('page_on_front') == $post->ID )
			$url = trailingslashit( get_permalink() ) . user_trailingslashit( $wp_rewrite->pagination_base . "/$page", 'single_paged' );
		else
			$url = trailingslashit( get_permalink() ) . user_trailingslashit( $page, 'single_paged' );
	}

	return $url;
}
endif;