<?php
/**
 * Archive Template for Custom Post Types
**/
 
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit; ?>
	<div class="tan-break property-search grey-back" >
		<div class='container'>
			<div class='row'>
				<div class="col-md-1 col-sm-0"></div>
				<div class="col-md-11 col-sm-12">
					<h2>Property Search</h2>
				</div>
			</div>
			<div class='row' id='search-row'>
				<script>var search_row = document.getElementById('search-row'); search_row.className = 'row hidden-search';</script>
				<div class="col-md-1"></div>
				<div class="col-md-8">
					<?php dynamic_sidebar('sidebar-search'); ?>
				</div>
				<div class="col-md-3 submit-search">

				</div>
			</div>
		</div>
	</div>
	<div class="property-list" >
		<div class='container'>
			<div class='row'>
				<?php
				if ( have_posts() ) : ?>
				<div class="col-md-12">
					<header class="archive-header entry-header loop-header">
						<h4 class="">
							<?php
								$total_found = $GLOBALS['wp_query']->found_posts;
								$post_count = $GLOBALS['wp_query']->post_count;
								echo "Displaying <span>". $post_count . "</span> of ". $total_found ."  ";
								the_post();
								 
								if ( is_tax() && function_exists( 'epl_is_search' ) && false == epl_is_search() ) { // Tag Archive
									$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
									$title = sprintf( __( 'Property in %s', 'epl' ), $term->name );
								}
								else if ( function_exists( 'epl_is_search' ) && epl_is_search() ) { // Search Result
									$title = __( 'Search Result', 'epl' );
								}
								
								else if ( function_exists( 'is_post_type_archive' ) && is_post_type_archive() && function_exists( 'post_type_archive_title' ) ) { // Post Type Archive
									//$title = post_type_archive_title( '', false );
									$title = " Results";
								} 
								
								else { // Default catchall just in case
									$title = __( 'Listing', 'epl' );
								}
								
								if ( is_paged() )
									printf( '%s &ndash; Page %d', $title, get_query_var( 'paged' ) );
								else
									echo $title;
								
								rewind_posts();
							?>
						</h4>
					</header>
				</div>
			</div>
			<?php $out = ""; $i = 0; while ( have_posts() ) : // The Loop
				if ($i%2 == 0 ){
					$class = " ";
					$out .= "<div class='row'>";									
				}
				else{
					$class = " ";
				}
				the_post();
				$thumbnail_id = get_post_thumbnail_id( $post->ID );
				$image = wp_get_attachment_image_src( $thumbnail_id , 'large' );
				$out .= 
					"<div class='col-md-6 col-xs-12 prop-container ".$class."'>
						<a class='no-dec' href='" . get_permalink() . "'>
							<div class='list-prop black-back'>
								<div class='prop-image' style='background-image: url( ". $image[0] ." )'></div>
								<div class='details'>
									<div class='title'>" . get_the_title( $post->ID ) . "</div>
									<div class='specs'>
										<div class='row'>
											<div class='col-md-4'>
												<label>PRICE</label>$".number_format( floatval($property->meta['property_price'][0]), 0, '.', ',')."
											</div>
											<div class='col-md-3'>
												<label>BED</label>".$property->meta['property_bedrooms'][0]."
											</div>
											<div class='col-md-3'>
												<label>BATH</label>".$property->meta['property_bathrooms'][0]."
											</div>
										</div>
									</div><!--/spec-->
								</div>
							</div><!--/list-prop-->
						<a/>
					</div><!--/prop-container-->";
					if ($i%2 != 0 ){
						$out .= "</div>";									
					}
					global $property;
					$i++;
					endwhile; // end of one post
					echo $out;
				?>
				<div class="col-md-12">
					<div class='search-nav'>
						<?php  gf_pagination(); ?>
					</div>
				</div>
				<?php else: ?>
					<div class="hentry">
						<div class="entry-header clearfix">
							<h3 class="entry-title"><?php _e('Listing not Found', 'epl'); ?></h3>
						</div>
						
						<div class="entry-content clearfix">
							<p><?php _e('Listing not found, expand your search criteria and try again.', 'epl'); ?></p>
						</div>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
<?php get_footer(); ?>